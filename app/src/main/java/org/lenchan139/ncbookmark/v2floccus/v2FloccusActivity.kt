package org.lenchan139.ncbookmark.v2floccus

import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_v2_floccus.*
import kotlinx.android.synthetic.main.content_v2_floccus.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import org.jsoup.Connection
import org.jsoup.nodes.Document
import org.lenchan139.ncbookmark.Class.*
import org.lenchan139.ncbookmark.Constants
import org.lenchan139.ncbookmark.MainActivity
import org.lenchan139.ncbookmark.R
import java.io.IOException

class v2FloccusActivity : AppCompatActivity() {

    internal var urlNt: String? = null
    internal var username: String? = null
    internal var password: String? = null
    internal lateinit var login: String

    internal lateinit var floccusHelper : FloccusHelper
    internal var isFullyInit = false
    lateinit var settings : SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_v2_floccus)
        setSupportActionBar(toolbar)



        val window = window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = resources.getColor(R.color.colorPrimaryDark)
        urlNt = intent.getStringExtra("url")
        username = intent.getStringExtra("username")
        password = intent.getStringExtra("password")
        login = username + ":" + password
        settings = getSharedPreferences("data",0)
        if (urlNt != null && username != null && password != null) {
            startUpdateFolderAsync()
        } else {
            Toast.makeText(this, "Plase login first!", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

        fab.setOnClickListener { view ->
           openAddBookmarkActivity()
        }
    }

    override fun onPostResume() {
        super.onPostResume()
        if(isFullyInit)
            updateFoldersFromDatabase(floccusHelper.lastFolderId)
    }

    override fun onBackPressed() {
            exitActivity()

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.bookmark_view, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_reset) {
            settings.edit().clear().commit()
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()
            return true
        } else if (id == R.id.add_bookmark) {
            openAddBookmarkActivity()
        }else if( id == R.id.refresh){
            updateFoldersFromDatabase(floccusHelper.lastFolderId)
        }

        return super.onOptionsItemSelected(item)
    }
    fun exitActivity(){
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Exit?")
        dialog.setMessage("Are you sure to exit NCBookmark?")
        dialog.setPositiveButton("Exit", DialogInterface.OnClickListener { dialog, which ->
            finish()
        })
        dialog.setNegativeButton("Cancel",null)
        dialog.create().show()
    }
    fun openAddBookmarkActivity(){
        val intent = Intent(this, AddBookmarkActivityV2Floccus::class.java)
        intent.putExtra("username", getIntent().getStringExtra("username"))
        intent.putExtra("password", getIntent().getStringExtra("password"))
        intent.putExtra("url", getIntent().getStringExtra("url"))
        //intent.putExtra("inTag", arrayFloccusHelper.getFolderPathName())
        startActivity(intent)
    }

    fun updateFolderGUI(){
    }
    fun startUpdateFolderAsync() {
        listview.adapter = null
        progressBar.visibility = View.VISIBLE
        fetchFolderList()
    }
    fun fetchFolderList(){
        var result: String? = null

        val base64login = String(Base64.encode(login.toByteArray(), 0))
        doAsync {
            val dlUrl = String.format("%s%s%s", urlNt, Constants.V2_API_ENDPOINT, "folder")
            Log.v("v2floccus dl url", dlUrl)
            try {
                result = Constants.getDefaultJsoup(Connection.Method.GET, dlUrl, base64login)
                        .execute().parse().body().text()
            } catch (e: IOException) {
                e.printStackTrace()
            }catch(e:IllegalArgumentException){
                e.printStackTrace()
            }
            Log.v("v2floccus folder json", result)
            uiThread { updateFolders(result) }
        }
    }
    fun updateFolders(folderJsonString:String?){
        if(folderJsonString == null)
            return
        try {
            val json = JSONObject(folderJsonString)
            if(json["status"] == "success"){
                floccusHelper = FloccusHelper(this, folderJsonString)
                updateFoldersFromDatabase("-1")
            }else{
                toast("Fetch Folder list failed.").show()
            }


        }catch (e:Exception){
            e.printStackTrace()
            toast(e.localizedMessage).show()
        }

    }
    fun updateFoldersFromDatabase(folderId: String){

        progressBar.visibility = View.VISIBLE
        val list = floccusHelper.getDisplayList(folderId)
        val arrListItems = ArrayList<FloccusListviewItem>()
        if(!folderId.equals("-1")){
            val currFolder = floccusHelper.getCurrentFolderInfo(folderId)
            arrListItems.add(FloccusListviewItem(FloccusListviewItemConst().ITEM_TYPE_FOLDER, "..", currFolder.parend_id, null))
        }

        Log.v("v2floccus parent size", list.size.toString())
        for(i in list){
            Log.v("v2floccus parent", i.title)
            arrListItems.add(FloccusListviewItem(FloccusListviewItemConst().ITEM_TYPE_FOLDER, i.title, i.id, null))

        }
        //updateListView(arrListItems)
        updateBookmark(arrListItems, folderId)
    }

    fun updateBookmark(arrayFloccusList:ArrayList<FloccusListviewItem>, folderId:String){
        var result = ""
        val base64login = String(Base64.encode(login.toByteArray(), 0))
        doAsync {
            val dlUrl = String.format("%s%s%s%s", urlNt, Constants.V2_API_ENDPOINT, "bookmark?folder=", folderId)
            Log.v("v2floccus dl url", dlUrl)
            try {
                result = Constants.getDefaultJsoup(Connection.Method.GET, dlUrl, base64login)
                        .execute().parse().body().text()
            } catch (e: IOException) {
                e.printStackTrace()
            }catch(e:IllegalArgumentException){
                e.printStackTrace()
            }
            Log.v("v2floccus folder json", result)
            uiThread { processBookmarkArray(arrayFloccusList, result) }
        }
    }
    fun processBookmarkArray(arrayFloccusList:ArrayList<FloccusListviewItem>, result: String){
        try{
            val jsonObj = JSONObject(result)
            if(jsonObj.has("data")){
                val jsonArray = jsonObj.getJSONArray("data")
                for(i in 0..jsonArray.length()-1){
                    val bmObj = jsonArray.getJSONObject(i)
                    val bookmark = BookmarkItemV2()
                    bookmark.title = bmObj.getString("title")
                    bookmark.url = bmObj.getString("url")
                    bookmark.id = bmObj.getString("id").toInt()
                    arrayFloccusList.add(FloccusListviewItem(FloccusListviewItemConst().ITEM_TYPE_BOOKMARK, bookmark.title!!, bookmark.url!!, bookmark))
                }
            }
        }catch (e:java.lang.Exception){
            e.printStackTrace()
        }

        updateListView(arrayFloccusList)
    }
    fun updateListView(arrayFloccusList:ArrayList<FloccusListviewItem>) {
        val displayArrayList = ArrayList<String>()
        for (i in arrayFloccusList) {
            if(i.type == FloccusListviewItemConst().ITEM_TYPE_FOLDER)
                displayArrayList.add(String.format("\uD83D\uDCC1 %s", i.title))
            else if(i.type == FloccusListviewItemConst().ITEM_TYPE_BOOKMARK)
                displayArrayList.add(String.format("\uD83D\uDD16 %s", i.title))
            else
                displayArrayList.add(String.format(" %s", i.title))
            Log.v("v2floccus title", i.title)
        }
        val listAdptTags = ArrayAdapter<String>(this@v2FloccusActivity, android.R.layout.simple_list_item_1, android.R.id.text1, displayArrayList)
        listview.adapter = listAdptTags

        listview.visibility = View.VISIBLE
        listview.onItemClickListener = AdapterView.OnItemClickListener {
            parent, view, position, id ->
            //openBookmarkView(strTags[position]!!)

            val onClick = arrayFloccusList.get(position)
            if(onClick.type==FloccusListviewItemConst().ITEM_TYPE_FOLDER){
                Log.v("theTestUri1",onClick.uri)
                var curr_id = onClick.uri
                updateFoldersFromDatabase(curr_id)

            }else if(onClick.type==FloccusListviewItemConst().ITEM_TYPE_BOOKMARK){
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(onClick.uri))
                startActivity(intent)
            }

        }
        listview.onItemLongClickListener = AdapterView.OnItemLongClickListener { parent, view, position, id -> true
            val dialog = AlertDialog.Builder(this)
            val strOptions = arrayOf("Open", "Edit", "Delete")

            val bookmarkObj = arrayFloccusList.get(position).bookmarkItem
            if(bookmarkObj != null) {
                dialog.setTitle(bookmarkObj.title)
                dialog.setItems(strOptions) { boolean1, which ->
                    if (which == 0) {
                        //open
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(bookmarkObj.url))
                        startActivity(intent)
                    } else if (which == 1) {
                        //edit
                        //Toast.makeText(BookmarkViewActivityV2.this, "Function not yet.", Toast.LENGTH_SHORT).show();
                        val intent = Intent(this, EditBookmarkActivityV2Floccus::class.java)
                        intent.putExtra("username", getIntent().getStringExtra("username"))
                        intent.putExtra("password", getIntent().getStringExtra("password"))
                        intent.putExtra("url", getIntent().getStringExtra("url"))
                        intent.putExtra("id", bookmarkObj.id)
                        startActivity(intent)
                    } else if (which == 2) {
                        //delete
                        val dialogD = AlertDialog.Builder(this)
                        dialogD.setMessage("Delete This:" + bookmarkObj.title +
                                "(Url: " + bookmarkObj.url + ") ?")
                        dialogD.setTitle("Delete Action")
                        dialogD.setNegativeButton("No") { dialog, which -> }
                        dialogD.setPositiveButton("Yes, Sure.") { dialog, which ->
                            val deleteTask = deleteTask(bookmarkObj.id)

                        }
                        dialogD.show()
                    }
                }
                dialog.show()
            }else{
                // it is not bookmark
            }
            true
        }

        isFullyInit = true
        progressBar.visibility = View.GONE


    }

    fun deleteTask( dId:Int){
        var deleteId = dId
         lateinit var result: Document
         val urlSe = Constants.V2_API_ENDPOINT + "bookmark/" + deleteId
         val base64login = String(Base64.encode(login.toByteArray(), 0))
        if (deleteId != -1) {
            doAsync {
                try {
                    result = Constants.getDefaultJsoup(Connection.Method.DELETE, urlNt+urlSe, base64login)
                            .execute().parse()
                    deleteId = -3
                } catch (e: IOException) {
                    e.printStackTrace()
                    deleteId = -9
                }

                uiThread {
                if (deleteId == -1) {
                } else if (deleteId == -3) {
                    toast("Deletion Done.").show()
                    updateFoldersFromDatabase(floccusHelper.lastFolderId)
                } else if (deleteId == -9) {
                 toast("Something was wrong when deletion.").show()
                }
                //deleteId = -1
                }
            }
        }

    }
}
