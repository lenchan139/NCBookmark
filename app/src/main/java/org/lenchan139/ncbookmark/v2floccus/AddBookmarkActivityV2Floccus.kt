package org.lenchan139.ncbookmark.v2floccus

import android.app.Dialog
import android.content.DialogInterface
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.content_add_bookmark_activity_v2floccus.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.jsoup.Connection
import org.jsoup.nodes.Document
import org.lenchan139.ncbookmark.Class.FloccusHelper
import org.lenchan139.ncbookmark.Class.TagsItem
import org.lenchan139.ncbookmark.Constants
import org.lenchan139.ncbookmark.R

import java.io.IOException
import java.net.URL

class AddBookmarkActivityV2Floccus : AppCompatActivity() {

    internal var urlNt: String? = null
    internal var username: String? = null
    internal var password: String? = null
    internal lateinit var tags: Array<String?>
    internal lateinit var login: String
    internal lateinit var edtUrl: EditText
    internal lateinit var edtTitle: EditText
    internal lateinit var edtDescr: EditText
    internal lateinit var edtTag: EditText
    internal lateinit var btnSelectTag: Button
    internal lateinit var folderSelectDialog: Dialog
    internal lateinit var btnSubmit: Button
    internal lateinit var floccusHelper : FloccusHelper
    internal var arrDisplayFolderList = ArrayList<String>()
    internal var arrIdFolderList = ArrayList<String>()
    internal var selectedFolderId = "-1"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_bookmark_v2floccus)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        edtDescr = findViewById(R.id.description)
        edtTag = findViewById(R.id.tag)
        edtTitle = findViewById(R.id.title)
        edtUrl = findViewById(R.id.url)
        btnSelectTag = findViewById(R.id.selectTag)
        btnSubmit = findViewById(R.id.submit)

        val window = window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = resources.getColor(R.color.colorPrimaryDark)
        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
        fab.visibility = View.GONE
        //for out call use
        var inUrl = intent.getStringExtra("inUrl")
        var inTitle = intent.getStringExtra("inTitle")
        var inTag = intent.getStringExtra("inTag")
        //
        var sp = getSharedPreferences("data", 0)
        urlNt = sp.getString("url",null)
        username = sp.getString("username",null)
        password = sp.getString("password",null)
        var apiVer = sp.getInt("apiVersion",-1)
        login = username + ":" + password
        if (urlNt != null && username != null && password != null && apiVer >= 0) {
            btnSubmit.setOnClickListener { AddBookmarkTask().execute() }
        } else {
            Toast.makeText(this,"maybe you are not login yet.",Toast.LENGTH_SHORT).show()
            finish()
        }

        if(inUrl!=null){
            edtUrl.setText(inUrl)
        }
        if(inTag!=null){
            edtTag.setText(inTag)
        }
        if(inTitle!=null){
            edtTitle.setText(inTitle)
        }

        initFolderDialog()
        startFetchFolder()
        selectFolder.setOnClickListener {  initFolderDialog(); folderSelectDialog.show() }
        btnSelectTag.setOnClickListener { fetchTagsTask().execute() }
        btnSubmit.setOnClickListener { AddBookmarkTask().execute() }
    }

    private inner class fetchTagsTask : AsyncTask<URL, Int, Long>() {
        internal var urlSe = Constants.V2_API_ENDPOINT + "tag"
        internal val base64login = String(Base64.encode(login.toByteArray(), 0))
        internal lateinit var result: Document

        override fun doInBackground(vararg params: URL): Long? {

            try {
                result = Constants.getDefaultJsoup(Connection.Method.GET, urlNt!! + urlSe, base64login)

                        .execute().parse()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return null
        }
        override fun onPostExecute(aLong: Long?) {
            Log.v("testAllTags", result.body().text())
            var err_check = true
            try {
                val jsonArray = JSONArray(result.body().text())
                tags = arrayOfNulls<String>(jsonArray.length())

                for (i in 0..jsonArray.length() - 1) {
                    tags[i] = jsonArray.get(i).toString()
                    Log.v("testTagConverting", jsonArray.get(i).toString())
                }

            } catch (e: JSONException) {
                e.printStackTrace()
                err_check = false
            }

            if (err_check) {
                val dialog = AlertDialog.Builder(this@AddBookmarkActivityV2Floccus)
                dialog.setTitle("Exciting Tag")
                dialog.setItems(tags) { dialog, which -> edtTag.setText(tags!![which]) }
                dialog.setNegativeButton("Cancel", null)
                dialog.show()
            }
            super.onPostExecute(aLong)
        }

    }

    private inner class AddBookmarkTask : AsyncTask<URL, Int, Long>() {
        internal lateinit var tag: Array<String>
        internal var url: String? = null
        internal var title: String? = null
        internal var des: String? = null
        internal lateinit var result: Document
        internal var no_error = true
        internal var error_msg: String? = null
        internal var urlSe = Constants.V2_API_ENDPOINT + "bookmark"
        internal val base64login = String(Base64.encode(login.toByteArray(), 0))

        override fun onPreExecute() {
            val tampTag = TagsItem().strToArray(edtTag.text.toString())
            if(tampTag!= null){
                tag = tampTag
            }
            url = edtUrl.text.toString()
            title = edtTitle.text.toString()
            des = edtDescr.text.toString()
            if ( url == null || title == null) {
                no_error = false
                Toast.makeText(this@AddBookmarkActivityV2Floccus, "Please tag, url and title cannot be empty.", Toast.LENGTH_SHORT).show()
            }
            if (des == null) {
                des = ""
            }
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: URL): Long? {
            if (no_error) {
                Log.v("add or edit bm", urlNt+urlSe)
                try {
                    var jsoup = Constants.getDefaultJsoup(Connection.Method.POST, urlNt + urlSe, base64login)
                            .data("url", url)
                            .data("title", title)
                            .data("description", des)
                            .data("folders[]", selectedFolderId)
                           for(i in 0..tag.size-1){
                               jsoup.data("item[tags][]", tag.get(i).trim())
                           }
                    result = jsoup.execute().parse()

                } catch (e: IOException) {
                    e.printStackTrace()
                    error_msg = e.message
                }

            }
            return null
        }

        override fun onPostExecute(aLong: Long?) {
            if (error_msg != null) {
                Toast.makeText(this@AddBookmarkActivityV2Floccus, error_msg, Toast.LENGTH_SHORT).show()
            } else {
                Log.v(" v2floccus result body", result.body().text())
                Toast.makeText(this@AddBookmarkActivityV2Floccus, "Bookmark Added!", Toast.LENGTH_SHORT).show()
                this@AddBookmarkActivityV2Floccus.finish()
            }
            super.onPostExecute(aLong)
        }
    }
    fun initFolderDialog(){
        val array = arrDisplayFolderList
        folderSelectDialog = AlertDialog.Builder(this)
        .setTitle("Choose Exist Folder[${arrDisplayFolderList.size}]")
        .setItems(arrDisplayFolderList.toTypedArray(), DialogInterface.OnClickListener { dialog, which ->
            selectedFolderId = arrIdFolderList[which]
            folder.setText(arrDisplayFolderList[which])
        })
        .setNegativeButton("Cancel", null)
                .create()
    }
    fun startFetchFolder(){
        var result: String? = null

        val base64login = String(Base64.encode(login.toByteArray(), 0))
        doAsync {
            val dlUrl = String.format("%s%s%s", urlNt, Constants.V2_API_ENDPOINT, "folder")
            Log.v("v2floccus dl url", dlUrl)
            try {
                result = Constants.getDefaultJsoup(Connection.Method.GET, dlUrl, base64login)
                        .execute().parse().body().text()
            } catch (e: IOException) {
                e.printStackTrace()
            }catch(e:IllegalArgumentException){
                e.printStackTrace()
            }
            Log.v("v2floccus1 folder json", result)
            uiThread {
                if(result == null)
                    //
                else if(result!!.isEmpty())
                    toast("Failed! Please update your nextcloud and nextcloud bookmark version.")
                else
                    updateFolders(result!!)
            }
        }
    }
    fun updateFolders(folderJsonString:String){
        try {
            val json = JSONObject(folderJsonString)
            if(json["status"] == "success"){
                floccusHelper = FloccusHelper(this, folderJsonString)
                doAsync {
                    arrDisplayFolderList.add("!Root")
                    arrIdFolderList.add("-1")
                    updateFoldersList("├","-1")
                }
            }else{
                toast("Fetch Folder list failed.").show()
            }


        }catch (e:Exception){
            e.printStackTrace()
            toast(e.localizedMessage).show()
        }
    }
    fun updateFoldersList(prefix: String, folderId:String){
            val list = floccusHelper.getDisplayList(folderId)
            for(i in list){
                arrDisplayFolderList.add("$prefix ${i.title}")
                arrIdFolderList.add(i.id)

                Log.v("v2floccus1 folder json", "${i.id} | ${i.title}")
                if(floccusHelper.getDisplayList(i.id).isNotEmpty()){
                    val newPrefix = prefix + "├"
                    updateFoldersList(newPrefix, i.id)
                }

        }


    }
}
