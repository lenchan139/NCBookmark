package org.lenchan139.ncbookmark.Class

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception

class FloccusHelper(context:Context, val initJson:String):SQLiteOpenHelper(context, null, null, 1){
    val tableName = "folders"
    var lastFolderId = "-1"
    override fun onCreate(db: SQLiteDatabase?) {
        //
        val sql = "CREATE TABLE $tableName (" +
                "  `id` int(11) PRIMARY KEY NOT NULL," +
                "  `parent_id` int(11) NOT NULL," +
                "  `title` text NOT NULL" +
                "  );"
        db!!.execSQL(sql)
        try{
            val json  = JSONObject(initJson)
            if(json.getString("status") == "success"){
                writeToDatabase(db, json.getJSONArray("data"))
            }
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
       //
    }

    fun writeToDatabase(database:SQLiteDatabase?, jsonArray: JSONArray){
        var db = database
        for(i in 0..jsonArray.length()-1){
            val values = ContentValues()
            val obj = jsonArray.getJSONObject(i)
            values.put("id", obj.getString("id"))
            values.put("parent_id", obj.getString("parent_folder"))
            values.put("title", obj.getString("title"))
            val children = obj.getJSONArray("children")
            if(db == null)
                db = writableDatabase
            db!!.insert(tableName, null, values)
            if(children.length() > 0){
                writeToDatabase(db, children)
            }
        }
    }
    fun getDisplayList(folderId:String):ArrayList<FolderItem>{
        val cursor = readableDatabase.query(tableName, arrayOf("id", "parent_id", "title"), "parent_id = ?", arrayOf(folderId), null, null, null)
        val arrFolders = ArrayList<FolderItem>()
        lastFolderId = folderId
        try {
            if(cursor.moveToFirst()){
                do {
                    val id = cursor.getInt(cursor.getColumnIndex("id"))
                    val parentId = cursor.getInt(cursor.getColumnIndex("parent_id"))
                    val title = cursor.getString(cursor.getColumnIndex("title"))
                    val item = FolderItem("$id", "$parentId", title)
                    arrFolders.add(item)
                } while(cursor.moveToNext())

            }
        } catch (e:Exception) {
            e.printStackTrace()
        } finally {
            if(cursor != null && !cursor.isClosed){
                cursor.close()
            }
        }
        return arrFolders
    }

    fun getCurrentFolderInfo(folderId:String):FolderItem{
        val cursor = readableDatabase.query(tableName, arrayOf("id", "parent_id", "title"), "id = ?", arrayOf(folderId), null, null, null)
        val arrFolders = ArrayList<FolderItem>()
        var item = FolderItem("", "", "")
        try {
            if(cursor.moveToFirst()){
                val id = cursor.getInt(cursor.getColumnIndex("id"))
                val parentId = cursor.getInt(cursor.getColumnIndex("parent_id"))
                val title = cursor.getString(cursor.getColumnIndex("title"))
                item = FolderItem("$id", "$parentId", title)


            }
        } catch (e:Exception) {
            e.printStackTrace()
        } finally {
            if(cursor != null && !cursor.isClosed){
                cursor.close()
            }
        }
        return item
    }
}