package org.lenchan139.ncbookmark

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.*
import kotlinx.android.synthetic.main.content_main.*
import org.json.JSONException
import org.json.JSONObject
import org.jsoup.Connection
import org.jsoup.HttpStatusException

import org.lenchan139.ncbookmark.v1.TagViewActivity
import org.lenchan139.ncbookmark.v2.TagListActivityV2
import org.lenchan139.ncbookmark.v2floccus.v2FloccusActivity
import org.lenchan139.ncbookmark.v2floccusLegacy.V2FloccusViewActivity
import java.io.FileNotFoundException
import java.io.IOException
import java.io.PrintWriter
import java.io.StringWriter
import java.net.UnknownHostException
import java.util.ArrayList

class MainActivity : AppCompatActivity() {
    internal lateinit var edtUrl: EditText
    internal lateinit var edtUsername: EditText
    internal lateinit var edtPassword: EditText
    internal lateinit var btnSave: Button
    internal lateinit var sp: SharedPreferences
    internal lateinit var url: String
    internal lateinit var username: String
    internal lateinit var password: String
    internal lateinit var spinnerApi : Spinner
    internal lateinit var spinnerUrlPrefix : Spinner
    val apiString = Constants.apiStringArray
    val urlPrefixArray = arrayOf("https://","http://")
    internal var urlSe = Constants.V2_API_ENDPOINT + "bookmark"

    val debugLogs = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sp = getSharedPreferences("data", 0)

        edtUrl = findViewById<EditText>(R.id.url)
        edtUsername = findViewById<EditText>(R.id.username)
        edtPassword = findViewById<EditText>(R.id.password)
        btnSave = findViewById<Button>(R.id.save)
        debug.setOnClickListener { onShowDebugLogsDialog(debugLogs.joinToString("\n","", "", -1, "", null)) }
        spinnerApi = findViewById<Spinner>(R.id.spinnerApi)
        spinnerUrlPrefix = findViewById<Spinner>(R.id.spinnerUrl)
        val listAdapterApi = ArrayAdapter(this@MainActivity,
                android.R.layout.simple_spinner_dropdown_item,
                apiString)
        val listAdapterUrl = ArrayAdapter(this@MainActivity,
                android.R.layout.simple_spinner_dropdown_item,
                urlPrefixArray)
        spinnerApi.adapter = listAdapterApi
        spinnerUrlPrefix.adapter = listAdapterUrl

        val apiVer = sp.getInt("apiVersion", apiString.lastIndex )
        spinnerApi.setSelection(apiVer)
        url = sp.getString("url", "")
        username = sp.getString("username", "")
        password = sp.getString("password", "")
        val  lastInternalVersion = sp.getInt("lastInternalVersion", 0)
        Log.v("v2v2v2", lastInternalVersion.toString())
        if (url != "" && username != "" && password != "" && lastInternalVersion >= Constants.INTERNAL_VERSION) {
            startActivity(startBookmarkView(this, url, username, password, spinnerApi))
            finish()
        } else {
            btnSave.setOnLongClickListener {
                //Toast.makeText(MainActivity.this, spinnerApi.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                Log.v("urlHere",url)
                url = edtUrl.text.toString()
                if(url.startsWith("http")){
                    url.replace("https://","").replace("http://","")
                }
                if(url.lastIndexOf("/") == url.length-1){
                    url += '/'
                }
                url = urlPrefixArray.get(spinnerUrlPrefix.selectedItemPosition) + edtUrl.text.toString()
                username = edtUsername.text.toString()
                password = edtPassword.text.toString()
                Toast.makeText(this@MainActivity, "Saved.", Toast.LENGTH_SHORT).show()
                sp.edit().putString("url", url).commit()
                sp.edit().putString("username", username).commit()
                sp.edit().putString("password", password).commit()
                startActivity(startBookmarkView(this@MainActivity, url, username, password, spinnerApi))
                finish()
                true
            }
            btnSave.setOnClickListener {
                sp.edit().putInt("lastInternalVersion", Constants.INTERNAL_VERSION).commit()
                //Toast.makeText(MainActivity.this, spinnerApi.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                Log.v("urlHere",url)
                url = edtUrl.text.toString()
                if(url.startsWith("http")){
                    url.replace("https://","").replace("http://","")
                }
                if(url.lastIndexOf("/") == url.length-1){
                    url += '/'
                }
                url = urlPrefixArray.get(spinnerUrlPrefix.selectedItemPosition) + edtUrl.text.toString()
                username = edtUsername.text.toString()
                password = edtPassword.text.toString()
                DlTask().execute()

            }
        }

    }

    fun startBookmarkView(activity: Activity, url: String, username: String, password: String, spinner: Spinner): Intent? {
        btnSave.setEnabled(false)
        if (spinner.selectedItem.toString().equals("v1")) {
            val intent = Intent(activity, TagViewActivity::class.java)
            intent.putExtra("url", url)
            intent.putExtra("username", username)
            intent.putExtra("password", password)
            sp.edit().putInt("apiVersion", spinnerApi.selectedItemPosition).apply()
            return intent
        } else if (spinner.selectedItem.toString().equals("v2")) {
            val intent = Intent(activity, TagListActivityV2::class.java)
            intent.putExtra("url", url)
            intent.putExtra("username", username)
            intent.putExtra("password", password)
            sp.edit().putInt("apiVersion", spinnerApi.selectedItemPosition).apply()
            return intent
        } else if(spinner.selectedItem.toString().equals("v2 with Floccus(legacy)")) {
            val intent = Intent(activity, V2FloccusViewActivity::class.java)
            intent.putExtra("url", url)
            intent.putExtra("username", username)
            intent.putExtra("password", password)
            sp.edit().putInt("apiVersion", spinnerApi.selectedItemPosition).apply()
            return intent
        } else if(spinner.selectedItem.toString().equals("v2 Floccus(Folder API)")) {
            val intent = Intent(activity, v2FloccusActivity::class.java)
            intent.putExtra("url", url)
            intent.putExtra("username", username)
            intent.putExtra("password", password)
            sp.edit().putInt("apiVersion", spinnerApi.selectedItemPosition).apply()
            return intent
        } else{
            return null
        }

    }

    internal inner class DlTask : AsyncTask<String, Int, Int>() {
        var result: String? = null
        val login = username + ":" + password
        var isConnected = false
        var isValid = false
        var errorMessage : String? = null
        val base64login = String(Base64.encode(login.toByteArray(), 0))
        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: String): Int? {
            val tempUrl = url + urlSe
            debugLogs.add(String.format("\ntry connect to [%s]\n",tempUrl))
            Log.v("requestUrl",tempUrl)

            val sw = StringWriter()
            val pw = PrintWriter(sw)
            try {
                val r = Constants.getDefaultJsoup(Connection.Method.GET, tempUrl, base64login)
                        .execute().parse()
                isConnected = true
                if(r != null){
                    result = r.body().text()
                    val status = JSONObject(result).get("status")
                    if(status.equals("success")) {
                        isValid = true
                    }else{
                        throw JSONException("fetch status failed")
                    }
                }
                Log.v("JSON_FOR_VALIDATE_ACC",r.toString())
                //val json = JSON

                debugLogs.add(String.format("\nsuccess.return body: ===\n%s\n===",r.toString()))
                isConnected = true
            }catch (e: HttpStatusException) {
                e.printStackTrace(pw)
                errorMessage = "It seems url, username or password is invalid."
                debugLogs.add(String.format("\nerror message: %s\n system error message: %s\n%s",errorMessage, e.message, sw.toString()))
            }catch(e:IllegalArgumentException){
                    e.printStackTrace(pw)
                    isConnected = false
                    errorMessage = "Incorrent URL, please correct it."
                debugLogs.add(String.format("\nerror message: %s\n system error message: %s\n%s",errorMessage, e.message, sw.toString()))
            }catch (e:UnknownHostException){
                e.printStackTrace(pw)
                isConnected = false
                errorMessage = "Unknown host on url, please correct it."
                debugLogs.add(String.format("\nerror message: %s\n system error message: %s\n%s",errorMessage, e.message, sw.toString()))
            }catch (e:FileNotFoundException){
                e.printStackTrace(pw)
                isConnected = true
                errorMessage = "FileNotFoundException()"
                debugLogs.add(String.format("\nerror message: %s\n system error message: %s\n%s",errorMessage, e.message, sw.toString()))
            } catch (e: IOException) {
                e.printStackTrace(pw)
                isConnected = false
                errorMessage = "Connection error, please try again later."
                debugLogs.add(String.format("\nerror message: %s\n system error message: %s\n%s",errorMessage, e.message, sw.toString()))
            }catch (e: JSONException) {
                e.printStackTrace(pw)
                isConnected = false
                errorMessage = "Authorization failed."
                debugLogs.add(String.format("\nerror message: %s\n system error message: %s\n%s",errorMessage, e.message, sw.toString()))
            }
            debugLogs.add("\nend connection \n============")
            Log.v("debugTrace", debugLogs.toString())
            return null
        }

        override fun onPostExecute(integer: Int?) {
            btnSave.setEnabled(true)
            if(errorMessage != null){
                Toast.makeText(this@MainActivity,errorMessage,Toast.LENGTH_SHORT).show()
                Log.v("error_type_on_validate", errorMessage)
            }else if (url.length > 0 && username.length > 0 && password.length > 0 && isConnected && isValid) {
                Toast.makeText(this@MainActivity, "Saved.", Toast.LENGTH_SHORT).show()
                sp.edit().putString("url", url).commit()
                sp.edit().putString("username", username).commit()
                sp.edit().putString("password", password).commit()
                startActivity(startBookmarkView(this@MainActivity, url, username, password, spinnerApi))
                finish()
            }else{
                Toast.makeText(this@MainActivity,"Something was wrong",Toast.LENGTH_SHORT).show()
            }
            super.onPostExecute(integer)
        }
    }
    fun onShowDebugLogsDialog(message:String){
        val dialog = AlertDialog.Builder(this)
                .setTitle("Debug Logs(be careful, this may include some private data)")
                .setMessage(message)
                .setNegativeButton("Close",null)
                .setNeutralButton("Share", DialogInterface.OnClickListener { dialog, which ->
                    val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
                    sharingIntent.type = "text/plain"
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share to...")
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, message)
                    startActivity(Intent.createChooser(sharingIntent, "Share to..."))
                })
                .create()
        dialog.show()
    }
}
