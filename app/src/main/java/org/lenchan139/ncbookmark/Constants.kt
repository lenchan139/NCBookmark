package org.lenchan139.ncbookmark

import org.jsoup.Connection
import org.jsoup.Jsoup

class Constants
{
    companion object {
        val INTERNAL_VERSION = 1
        val apiStringArray = arrayOf( "v1", "v2", "v2 with Floccus(legacy)",  "v2 Floccus(Folder API)" )
        val V2_API_ENDPOINT = "/apps/bookmarks/public/rest/v2/";
        val FLOCCUS_TAG_PREFIX = "floccus:"
        val ddg_url = "https://www.duckduckgo.com"
        val default_jsoup_timeout = 30*1000
        val default_user_agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.102 Safari/537.36 Vivaldi/2.0.1309.42"
        fun getDefaultJsoup(method:Connection.Method, url:String,base64login:String): Connection {
            return Jsoup.connect(url)
                    .referrer(ddg_url)
                    .userAgent(default_user_agent)
                    .ignoreHttpErrors(true)
                    .ignoreContentType(true)
                    .header("Authorization", "Basic " + base64login)
                    .method(method)
                    .timeout(default_jsoup_timeout)
        }
    }

}