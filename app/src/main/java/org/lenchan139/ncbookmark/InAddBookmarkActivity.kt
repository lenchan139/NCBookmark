package org.lenchan139.ncbookmark

import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import org.jetbrains.anko.toast
import org.lenchan139.ncbookmark.v2.AddBookmarkActivityV2
import org.lenchan139.ncbookmark.v2floccus.AddBookmarkActivityV2Floccus

class InAddBookmarkActivity : AppCompatActivity() {
    lateinit var sp : SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sp = getSharedPreferences("data", 0)

        val apiVer = sp.getInt("apiVersion", Constants.apiStringArray.lastIndex)
        val hostname = sp.getString("url", "")
        val username = sp.getString("username", "")
        val password = sp.getString("password", "")
        val inUrl = intent.getStringExtra("inUrl")
        val inTitle = intent.getStringExtra("inTitle")
        val inTag = intent.getStringExtra("inTag")
        Log.v("currentApi", apiVer.toString())
        if (hostname.isNotEmpty()
                && username.isNotEmpty()
                && password.isNotEmpty()
                && inUrl != null
                && inTitle != null
                && inTag != null)
        {
            if(apiVer == 3) {
                openAddBookmarkActivity(AddBookmarkActivityV2Floccus::class.java, inUrl, inTitle, inTag)
            }else if(apiVer == 2 || apiVer == 1){
                openAddBookmarkActivity(AddBookmarkActivityV2::class.java, inUrl, inTitle, inTag)
            }else{
                toast(getString(R.string.you_should_login_nc_bookmark_first))
                finish()
            }
        }else{
            toast(getString(R.string.you_should_login_nc_bookmark_first))
            finish()
        }
    }
    fun openAddBookmarkActivity(activity: Class<*>, inUrl:String, inTitle:String, inTag:String){
        val intent = Intent(this, activity)
        intent.putExtra("inUrl", inUrl)
        intent.putExtra("inTitle", inTitle)
        intent.putExtra("inTag", inTag)
        finish()
    }
}
